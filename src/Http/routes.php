<?php

Route::group([
    'as' => 'log::',
    'prefix' => config('scigeniq.dashboard.log.prefix'),
    'namespace' => '\Scigeniq\Log\Http\Controllers',
    'middleware' => config('scigeniq.dashboard.log.middleware')
], function(){

    Route::get('/log', [
        'as' => 'index',
        'uses' => 'LogController@index'
    ]);

});

